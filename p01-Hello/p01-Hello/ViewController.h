//
//  ViewController.h
//  p01-Hello
//
//  Created by Ciaran Slattery on 1/24/17.
//  Copyright © 2017 Ciaran Slattery. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController{
    UILabel *label;
}

@property (nonatomic, retain) IBOutlet UILabel *label;

-(IBAction) changeMessage;


@end

