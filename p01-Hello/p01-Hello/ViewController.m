//
//  ViewController.m
//  p01-Hello
//
//  Created by Ciaran Slattery on 1/24/17.
//  Copyright © 2017 Ciaran Slattery. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

@synthesize label;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)changeMessage
{
    [label setText:@"I said hello!"];
}

@end
